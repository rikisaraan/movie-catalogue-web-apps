import "../component/app-bar.js";
import "../component/movie-list.js";
import "../component/title-bar.js";
import "../component/modal-item.js";
import Movies from '../data/movies.js'

const main = () => {

    //deklarasi custom element
    const movieListElement = document.querySelector("movie-list");
    const appBar = document.querySelector("app-bar");
    const titleBar = document.querySelector("title-bar");

    const loadMoviePopular = async () => {
        try {
            //promise with async 
            const result = await Movies.getMoviePopular();
            titleBar.setAttribute("title", "Popular Movie");
            renderResult(result);
        } catch (message) {
            fallbackResult(message);
        }
    };

    const loadMovieNowPlaying = async () => {
        try {
            //promise with async 
            const result = await Movies.getMovieNowPlaying();
            titleBar.setAttribute("title", "Now Playing Movie");
            renderResult(result);
        } catch (message) {
            fallbackResult(message);
        }
    };

    const loadMovieUpComing = async () => {
        try {
            //promise with async 
            const result = await Movies.getMovieUpcoming();
            titleBar.setAttribute("title", "Upcoming Movie");
            renderResult(result);
        } catch (message) {
            fallbackResult(message);
        }
    };

    const loadMovieToprated = async () => {
        try {
            //promise with async 
            const result = await Movies.getMovieToprated();
            titleBar.setAttribute("title", "Top Rated Movie");
            renderResult(result);
        } catch (message) {
            fallbackResult(message);
        }
    };

    //search movie on button click function
    const onButtonSearchClicked = async () => {
        //promise with async 
        try {
            if (appBar.value == "") {
                Swal.fire({
                    title: 'Error!',
                    text: "Keywords cannot be empty",
                    icon: 'error',
                    confirmButtonText: 'Close'
                });
            } else {
                const result = await Movies.searchMovies(appBar.value);
                titleBar.setAttribute("title", "Searching Movie");
                renderResult(result);
            }
        } catch (message) {
            fallbackResult(message);
        }
    };


    const renderResult = results => {
        movieListElement.setMovies(results);
    };

    const fallbackResult = message => {
        movieListElement.renderError(message);
    };

    //load movie after buttoon search click
    appBar.clickEvent = onButtonSearchClicked;

    //load movie on title in appbar click
    appBar.home = loadMoviePopular;

    //load movie on popular movie menu
    appBar.popularMovie = loadMoviePopular;

    //load movie now playing movie menu
    appBar.nowPlayingMovie = loadMovieNowPlaying;

    //load movie upcoming movie menu
    appBar.upComingMovie = loadMovieUpComing;

    //load movie top rated movie menu
    appBar.topRatedMovie = loadMovieToprated;

    //load movie
    loadMoviePopular();
};

export default main; 