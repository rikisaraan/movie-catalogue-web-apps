class Movies {
    static searchMovies(keywords) {
        return fetch(`https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&query=${keywords}`)
            .then(response => {
                return response.json();
            })
            .then(resposeJson => {
                if (resposeJson.results) {
                    return Promise.resolve(resposeJson.results);
                } else {
                    return Promise.reject(`${keyword} not found`);
                }
            });
    }

    static getMoviePopular() {
        return fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&language=en-US`)
            .then(response => {
                return response.json();
            })
            .then(resposeJson => {
                if (resposeJson.results) {
                    return Promise.resolve(resposeJson.results);
                } else {
                    return Promise.reject(`${keyword} not found`);
                }
            });
    }

    static getMovieNowPlaying() {
        return fetch(`https://api.themoviedb.org/3/movie/now_playing?api_key=${API_KEY}&language=en-US&page=1`)
            .then(response => {
                return response.json();
            })
            .then(resposeJson => {
                if (resposeJson.results) {
                    return Promise.resolve(resposeJson.results);
                } else {
                    return Promise.reject(`${keyword} not found`);
                }
            });
    }

    static getMovieUpcoming() {
        return fetch(`https://api.themoviedb.org/3/movie/upcoming?api_key=${API_KEY}&language=en-US&page=1`)
            .then(response => {
                return response.json();
            })
            .then(resposeJson => {
                if (resposeJson.results) {
                    return Promise.resolve(resposeJson.results);
                } else {
                    return Promise.reject(`${keyword} not found`);
                }
            });
    }

    static getMovieToprated() {
        return fetch(`https://api.themoviedb.org/3/movie/top_rated?api_key=${API_KEY}&language=en-US&page=1`)
            .then(response => {
                return response.json();
            })
            .then(resposeJson => {
                if (resposeJson.results) {
                    return Promise.resolve(resposeJson.results);
                } else {
                    return Promise.reject(`${keyword} not found`);
                }
            });
    }
}

export default Movies;