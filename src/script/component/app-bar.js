class AppBar extends HTMLElement {

    constructor() {
        super();
    }

    connectedCallback() {
        this.render();
    }

    set clickEvent(event) {
        this._clickEvent = event;
        this.render();
    }

    set home(event) {
        this._clickHome = event;
        this.render();
    }

    set popularMovie(event) {
        this._clickPopularMovie = event;
        this.render();
    }

    set nowPlayingMovie(event) {
        this._clickNowPlayingMovie = event;
        this.render();
    }

    set upComingMovie(event) {
        this._clickUpComingMovie = event;
        this.render();
    }

    set topRatedMovie(event) {
        this._clickTopRatedMovie = event;
        this.render();
    }

    get value() {
        return this.querySelector("#searchInput").value;
    }

    render() {
        this.bgcolor = this.getAttribute("bgcolor") || null;
        this.title = this.getAttribute("title") || null;

        this.innerHTML = `
        <nav class="navbar navbar-expand-lg navbar-dark ${this.bgcolor} justify-content-between">            
            <a id="home" class="navbar-brand" href="#">${this.title}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul id="menu" class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Movie
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a id="popular" class="dropdown-item" href="#">Popular</a>
                            <a id="nowplaying" class="dropdown-item" href="#">Now Playing</a>
                            <a id="upcoming" class="dropdown-item" href="#">Upcoming</a>
                            <a id="toprated" class="dropdown-item" href="#">Top Rated</a>
                        </div>
                    </li>   
                </ul>
                <div class="form-inline my-2 my-lg-0">
                    <input id="searchInput" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button id="searchButton" class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </div>
            </div>
        </nav>`;


        this.querySelector("#searchButton").addEventListener("click", this._clickEvent);
        this.querySelector("#home").addEventListener("click", this._clickHome);
        this.querySelector("#popular").addEventListener("click", this._clickPopularMovie);
        this.querySelector("#nowplaying").addEventListener("click", this._clickNowPlayingMovie);
        this.querySelector("#upcoming").addEventListener("click", this._clickUpComingMovie);
        this.querySelector("#toprated").addEventListener("click", this._clickTopRatedMovie);
    }
}

customElements.define("app-bar", AppBar);