class FootBar extends HTMLElement {
    connectedCallback() {
        this.render();
    }

    render() {
        this.bgcolor = this.getAttribute("bgcolor") || null;
        this.title = this.getAttribute("title") || null;

        this.innerHTML = `        
        <div class="container pt-5">
            <div class="row fixed-bottom ${this.bgcolor}">
                <div class="col-12 text-white text-center py-2">
                    <small>${this.title}</small>
                </div>
            </div>
        </div>
        `;
    }
}

customElements.define("foot-bar", FootBar);