import "./movie-item.js";

class MovieList extends HTMLElement {

    constructor() {
        super();
    }

    setMovies(movies) {
        this._movies = movies;
        this.renderListMovie();
    }

    renderListMovie() {
        this.innerHTML = ``;
        this._movies.forEach(movie => {
            const movieItemElement = document.createElement("movie-item");
            movieItemElement.movie = movie;
            this.appendChild(movieItemElement);
        });

        const buttons = document.querySelectorAll(".btn-detail");
        buttons.forEach(button => {
            button.addEventListener("click", event => {
                const imageUrl = button.dataset.poster_path;
                const originalTitle = button.dataset.original_title;
                const overview = button.dataset.overview;

                document.querySelector(".modal-title").innerHTML = originalTitle;
                document.querySelector(".modal-body").innerHTML = `
                <div class="container-fluid">
                    <div class="row" >
                        <div class="col-lg-4 col-md-6 col-sm-12 mb-2" >
                            <img class="img-thumbnail card-img-top" src="${imageUrl}" alt="Card image cap">
                        </div>                        
                        <div  class="col-lg-8 col-md-12 col-sm-12 mb-2">
                        <p class='text-muted'>Overview</p>
                        <p>${overview}</p>
                        </div>                        
                    </div>
                </div>
                `;
            })
        })
    }

    renderError(message) {
        Swal.fire({
            title: 'Error!',
            text: message,
            icon: 'error',
            confirmButtonText: 'Close'
        })
    }
}

customElements.define("movie-list", MovieList);