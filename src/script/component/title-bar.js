class TitleBar extends HTMLElement {
    connectedCallback() {
        this.title = this.getAttribute("title") || "Popular Upcoming";
        this.render();
    }

    render() {
        this.innerHTML = `        
        <div class="row mt-3 pt-3 mb-3 pl-3 border-bottom">
            <h2>${this.title}</h2>
        </div>
        `;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue !== newValue) {
            this[name] = newValue;
        }
        this.render();
    }

    static get observedAttributes() {
        return ["title"];
    }
}

customElements.define("title-bar", TitleBar);