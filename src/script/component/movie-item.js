class MovieItem extends HTMLElement {
    set movie(movie) {
        this._movie = movie;
        this.renderMovieItem();
    }

    renderMovieItem() {
        const imageUrl = (this._movie.poster_path == null) ? `https://www.nextlevelfairs.com/assets/images/image-not-available.png` : `https://image.tmdb.org/t/p/w185${this._movie.poster_path}`;
        this.innerHTML = `
        <style>
            #movie-item{
                min-height:550px;
            }
            #poster{
                min-height:330px;                
            }       
        </style>
        <div class="col-lg-2 col-md-6 col-sm-12 mb-2 float-left">
            <div class="card shadow-sm p-2 mb-3 bg-white rounded" id="movie-item">
                <img id="poster" class="img-thumbnail card-img-top" src="${imageUrl}" alt="Card image cap">
                <div class="card-body">
                    <h6 class="card-title font-weight-bold">${this._movie.original_title}</h6>
                    <p class="card-text">${this._movie.release_date}</p>
                    <button
                        data-toggle="modal"
                        data-target="#detailModal"
                        data-id="${this._movie.id}"
                        data-original_title="${this._movie.original_title}"
                        data-poster_path="${imageUrl}"
                        data-overview="${this._movie.overview}"
                        class="btn btn-primary btn-detail">Detail
                    </button>
                </div>
            </div>
        </div>
        `;
    }
}

customElements.define("movie-item", MovieItem);